from flask import Flask,redirect,url_for,request,render_template
from google.auth import compute_engine
from google.cloud import datastore

credentials = compute_engine.Credentials()
ds=datastore.Client(credentials=credentials)

app=Flask(__name__)

@app.route('/success/<name>/<age>/<hobby>')
def success(name,age,hobby):
	return 'welcome {}, of age {} and hobby {}'.format(name,age,hobby)

@app.route('/')
def MainPage():
	return render_template('index.html')

@app.route('/gotDetails',methods=['POST','GET'])
def login():
	if request.method=='POST':
		user=request.form['nm']
		age=request.form['age']
		hobby=request.form['hobby']

		entity=datastore.Entity(key=ds.key("PersonDetails"))
		entity.update({
                        'name':user,
                        'age':age,
                        'hobby':hobby
                })
		ds.put(entity)

		return redirect(url_for('success', name=user,age=age,hobby=hobby))
	
	else:
		user=request.args.get('nm')
		age=request.args.get('age')
		hobby=request.args.get('hobby')

		entity=datastore.Entity(key=ds.key("PersonDetails"))
		entity.update({
                        'name':user,
                        'age':age,
                        'hobby':hobby
                })
		ds.put(entity)
		return redirect(url_for('success', name=user,age=age,hobby=hobby))


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500

if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END gae_flex_quickstart]
